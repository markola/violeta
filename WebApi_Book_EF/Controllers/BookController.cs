﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi_Book_EF.Models;

namespace WebApi_Book_EF.Controllers
{
    public class BookController : ApiController
    {
        private ApplicationDBContext db;

        public BookController()
        {
            db = new ApplicationDBContext();
        }

        //GET : api/book
        public IHttpActionResult GetBooks()
        {
            return Ok(db.Books.ToList());
        }

        //GET : api/book/5
        [HttpGet]
        public IHttpActionResult GetBook(int id)
        {
            Book book = db.Books.Find(id);

            if (book == null)
            {
                return NotFound();
            }

            return Ok(book);
        }

        //PUT : api/book/5
        public IHttpActionResult PutItem(int id, Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            Book bookFromDB = db.Books.Find(id);

            if (bookFromDB == null)
            {
                return NotFound();
            }

            bookFromDB.Name = book.Name;
            db.SaveChanges();

            return Ok(db.Books.ToList());
        }

        //POST : api/book
        public IHttpActionResult PostBook(Book book)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            db.Books.Add(book);                                                                                                                                                                                                                                                                                                                                        
            db.SaveChanges();

            return Ok(db.Books.ToList());
        }

        //DELETE : api/book/5
        public IHttpActionResult DeleteBook(int id)
        {
            Book book = db.Books.Find(id);

            if(book == null)
            {
                return NotFound();
            }

            db.Books.Remove(book);
            db.SaveChanges();
            return Ok(db.Books.ToList());

        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
        }
    }
}
