﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi_Book_EF.Models
{
    public class ApplicationDBContext : DbContext
    {
        public DbSet<Book> Books { get; set; }

    }
}